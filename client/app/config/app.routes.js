(() => {
    angular
        .module("GRC")
      
        .config(["$stateProvider","$urlRouterProvider",($stateProvider,$urlRouterProvider)=>{
            console.log("enter")
                $stateProvider
                    .state("searchDB",{
                        url:"/searchDB",
                        templateUrl:"app/search/searchDB.html",
                        controller:"SearchDBCtrl",
                        controllerAs:"ctrl",
                    })
                    .state("editWithParams",{
                        url:"/edit/:id",
                        templateUrl:"app/edit/edit.html",
                        controller:"EditCtrl",
                        controllerAs:"ctrl",
                    })
                    $urlRouterProvider.otherwise("/searchDB");
        }]);
})();