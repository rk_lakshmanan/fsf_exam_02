(function () {
    'use strict';
    angular
        .module("GRC")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "DeptService", "$stateParams", "$state"];

    function EditCtrl($filter, DeptService, $stateParams, $state) {

        var vm = this;

        vm.result = {};
        vm.item = {
            name: "",
            brand: "",
            upc12: "",
            id: ""
        }
        vm.save = save;
        vm.cancel = cancel
        vm.search = search;

        if ($stateParams.id) {
            vm.item.id = $stateParams.id;
            console.log("id is " + vm.item.id);
            vm.search()
        }

        function cancel(){
            $state.go("searchDB")
        }

        function save() {
            DeptService
                .saveChanges(vm.item)
                .then(function (result) {
                    console.log("saveChanges");
                    console.log(result.data);
                    // $state.go("searchDBWithParams",vm.item.id);
                    $state.go("searchDB");
                })
                .catch(function (err) {
                    console.log("saveChangesErr");
                    console.log(err);
                })
        }


        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");

            DeptService
                .retrieveShopListByID(vm.item.id)
                .then(function (result) {
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    var itemR = result.data[0];
                    vm.item.name = itemR.name;
                    vm.item.brand = itemR.brand;
                    vm.item.upc12 = itemR.upc12;

               })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        // // Switches editor state of the department name input/edit field
        // function toggleEditor() {
        //     vm.isEditorOn = !(vm.isEditorOn);
        // }
    }
})();