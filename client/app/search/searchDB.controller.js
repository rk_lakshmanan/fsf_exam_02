(function () {
    angular
        .module("GRC")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['DeptService', '$state'];

    function SearchDBCtrl(DeptService, $state) {
        var vm = this;

        vm.searchStringPN = '';
        vm.searchStringBrand = '';
        vm.result = null;
        vm.search = search;
        vm.shopList = [];
        vm.sortBy = "name";
        vm.asc = 'ASC';

        vm.editGo = (inputId) => {
            console.log('inputID: ' + inputId);
            $state.go("editWithParams", { id: inputId });
        }

        function search() {
            DeptService
                .retrieveShopList({
                    ssBrand: vm.searchStringBrand,
                    ssPN: vm.searchStringPN,
                    ssSortBy: vm.sortBy,
                    ssAsc: vm.asc
                })
                .then(function (results) {
                    vm.shopList = results.data;
                })
                .catch(function (err) {
                    console.log(err);
                    console.log("error " + err);
                });
        }

    }
})();