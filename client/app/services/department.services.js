// Always use an IIFE, i.e., (function() {})();
(function() {
    angular
        .module("GRC")
        .service("DeptService", DeptService);

    DeptService.$inject = ['$http'];

    function DeptService($http) {

        var service = this;

        service.retrieveShopList = retrieveShopList;
        service.retrieveShopListByID = retrieveShopListByID;
        service.saveChanges = saveChanges;
        function saveChanges(item) {
            return $http({
                method: 'PUT'
                , url: 'api/shopList/' + item.id
                , data: { item: item }
            })
        }

        function retrieveShopListByID(id) {
            return $http({
                method: 'GET'
                , url: 'api/shopList'
                , params: { id: id }
            });
        }

        function retrieveShopList(ss) {
          
            return $http({
                method: 'POST'
                , url: 'api/shopList'
                , data: {
                    'ss': ss,
                    // , params:{
                    // 'ssPN':ss.searchStringPN,
                    // 'ssBrand':ss.searchStringBrand,
                }
            });
        }
    }
})();