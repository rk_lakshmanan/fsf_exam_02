var updateShopListItem = function (db) {
  return function (req, res) {
    console.log(req.body.item)
    var i = req.body.item;
    db.ShopList
      .update(
      {
        brand: i.brand,
        name: i.name,
        upc12: i.upc12,
      }, {
        where: {
          id: i.id
        }
      })
      .then(function (item) {
        console.log("results:");
        console.log(item);
        res
          .status(200)
          .json(item);
      })
      .catch(function (err) {
        console.log("error clause: " + err);
        res
          .status(500)
          .json(err);
      })
  }

}

var retrieveShopListByID = function (db) {
  return function (req, res) {
    console.log("retrieveByID")
    console.log(req.query.id);
    db.ShopList
      .findAll({
        where: {
          id: req.query.id
        },
      })
      .then(function (item) {
        console.log("results:");
        console.log(item);
        res
          .status(200)
          .json(item);
      })
      .catch(function (err) {
        console.log("error clause: " + err);
        res
          .status(500)
          .json(err);
      })
  }
}
var retrieveShopList = function (db) {
  return function (req, res) {

    console.log(req.body.ss.ssBrand);
    console.log(req.body.ss.ssPN);
    console.log(req.body.ss.ssSortBy);
    console.log(req.body.ss.ssAsc);
    var orObj = [];
    if (req.body.ss.ssBrand)
      orObj.push({ brand: { $like: "%" + req.body.ss.ssBrand + "%" } });
    if (req.body.ss.ssPN)
      orObj.push({ name: { $like: "%" + req.body.ss.ssPN + "%" } });

    db.ShopList
      .findAll({

        where: {
          $or: orObj
          // $or: [
          //   { brand: { $like: "%" + req.body.ss.ssBrand + "%" } },
          //   { name: { $like: "%" + req.body.ss.ssPN + "%" } }
          // ]
        },
        limit: 20,
        order: [
          [req.body.ss.ssSortBy,req.body.ss.ssAsc],
          // ['name','ASC']
        ],

      })
      // db.sequelize.query('SELECT * FROM grocery_list WHERE brand LIKE brand'
      // +' '+'%'+ ss.ssBrand+'%',
      // { type: db.sequelize.QueryTypes.SELECT})

      .then(function (list) {
        console.log("results:");
        res
          .status(200)
          .json(list);
      })
      .catch(function (err) {
        console.log("error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};


// Export route handlers
module.exports = function (db) {
  return {
    retrieveShopList: retrieveShopList(db),
    retrieveShopListByID: retrieveShopListByID(db),
    updateShopListItem: updateShopListItem(db),
  }
};
