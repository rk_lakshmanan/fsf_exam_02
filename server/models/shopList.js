// TODO: 3.1 Define departments table model
// Model for departments table
// References:
// http://docs.sequelizejs.com/manual/installation/getting-started.html#your-first-model
// http://docs.sequelizejs.com/manual/tutorial/models-definition.html
module.exports = function (sequelize, DataTypes) {
  return sequelize.define("grocery_list", {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false
    },
    upc12: {
      type: DataTypes.BIGINT(12),
      // unique: true,
      allowNull: false
    },
    brand: {
      type: DataTypes.STRING(255),
      // unique: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      unique: true,
      allowNull: false
    },

  }, {
      // don't add timestamps attributes updatedAt and createdAt
      freezeTableName: true,
      timestamps: false,
    });
};
