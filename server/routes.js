// Handles API routes

module.exports = function(app, db) {
  var ShopList = require('./api/shopList.controller.js')(db);

  app.post("/api/shopList",ShopList.retrieveShopList);

  app.get("/api/shopList",ShopList.retrieveShopListByID)

  app.put("/api/shopList/:id",ShopList.updateShopListItem)
};
